CXX       := clang++
CXX_FLAGS := -std=c++17 -stdlib=libc++ -lSDL2 -g

BIN     := build
SRC     := src
INCLUDE := include

LIBRARIES   :=
EXECUTABLE  := fourmiz


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*