#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <vector>
#include <string>
#include <SDL2/SDL.h>

#include "fourmi.hpp"
#include "fourmiliere.hpp"
#include "plante.hpp"


class Board
{
    private:
        Fourmi fourmi;
        Fourmiliere fourmiliere;
        Plante plante;

    public:
        Board();
        int init_board();
};

#endif