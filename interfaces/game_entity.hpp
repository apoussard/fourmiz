#ifndef GAMEENTITY_H
#define GAMEENTITY_H


#include <iostream>

using namespace std;

class GameEntity {
    protected:
        int posX;
        int posY;
        int height;
        int width;

    public:
        void setWidth(int w) { width = w; }
        int getWidth() { return width; }
        void setHeight(int h) { height = h; }
        int getHeight() { return height; }
        void setPosX(int x) { posX = x; }
        int getPosX() { return posX; }
        void setPosY (int y) { posY = y; }
        int getPosY() { return posY; }
};

#endif